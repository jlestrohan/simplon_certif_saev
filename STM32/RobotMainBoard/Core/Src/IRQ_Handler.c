/*
 * IRQ_Handler.c
 *
 *  Created on: Feb 26, 2020
 *      Author: jack
 */

#include "main.h"
#include "button_handler.h"
#include "usart.h"
//#include "freertos_command_parser.h"
#include <string.h>

/**
 *
 * @param GPIO_Pin
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == B1_Pin)
	{
		buttonIRQ_cb();
	}
}


/**
 *
 * @param huart
 */
/*void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	//HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
	//char *msg="char hello";
	//HAL_UART_Transmit_IT(huart, (uint8_t *)msg, strlen(msg));
	cmd_parse_uart_cb(huart);
}*/
