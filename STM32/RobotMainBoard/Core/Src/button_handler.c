/*
 * button_handler.c
 *
 *  Created on: 26 févr. 2020
 *      Author: jack
 */

#include "button_handler.h"
#include "stdint.h"
#include <stdbool.h>
#include "gpio.h"

/**
 * USER CONFIG - Include your nucleo's model hal file here
 * ie: #include "stm32g0xx_hal.h"
 */
#include "stm32g4xx_hal.h"

/**
 * USER CONFIG - Button debouncing time
 * Delay within which a button press is considered as a "bounce" click
 */
#define BTN_DEBOUNCE_MS			40 // ms

uint32_t 	lastPressedTick = 0;
/**
 * returns true if not a bounce while releasing
 */
uint8_t buttonDebounce (uint32_t tick)
{
	return ((HAL_GetTick() - tick > BTN_DEBOUNCE_MS)  ? true : false);
}


/**
 * handles buttonIRQ events
 */
void buttonIRQ_cb() {

	if (buttonDebounce(lastPressedTick) || lastPressedTick == 0) {
		lastPressedTick = HAL_GetTick();
		HAL_GPIO_TogglePin(GPIOA, LD2_Pin);
		osEventFlagsSet( evt_usrbtn_id, BTN_PRESSED_FLAG);
	}
}


